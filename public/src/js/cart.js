$(document).ready(function(){
	$('.js-prod-del').on('click',function(e){
		e.preventDefault();
		var itemNum = $('.product__item').length;
		$(this).closest('.product__item').remove();
		if(itemNum == 1){
			$('.product__clear').addClass('is-active');
			$('.product__list, .product__total, .cart__list').addClass('is-hide');
		}
	})
});

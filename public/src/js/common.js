$(document).ready(function(){

	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);
		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		var headerH = $('.header').height();
		var footerH = $('.footer').outerHeight();
		if ($('body').attr('data-mobile') == 'false'){

			//gnb 1depth hover
			$('.gnb__depth1-item').on('mouseenter',function(){
				$(this).addClass('is-active');
			}).on('mouseleave',function(){
				$(this).removeClass('is-active');
			});
			//gnb 2depth hover
			$('.gnb__depth2-link').on('mouseenter',function(){
				$(this).addClass('is-active');
			}).on('mouseleave',function(){
				$(this).removeClass('is-active');
			});
			//global hover
			$('.global__link').on('mouseenter',function(){
				$(this).addClass('is-active');
			}).on('mouseleave',function(){
				$(this).removeClass('is-active');
			});
			$('.counsel').css('bottom','0');
		}else{
			var counselH = $('.counsel').outerHeight() + 1;
			$('.gnb__depth1-item, .global__link, .gnb__depth2-link').off('mouseenter');
			$('.js-depth2-open').off('click').on('click',function(e){
				e.preventDefault();
				$(this).closest('.gnb__depth1-item').toggleClass('is-active').find('.gnb__depth2-list').slideToggle(400);
				$(this).closest('.gnb__depth1-item').siblings().removeClass('is-active').find('.gnb__depth2-list').slideUp(400);
			});
			$('.counsel').css('bottom',-counselH);
		}
		console.log(footerH)
		$('body').css('padding-bottom',footerH);
	});


	var bodyY;
	function scrollStop(){
		bodyY = $(window).scrollTop();
		$('html, body').addClass("no-scroll");
		$('.common').css("top",-bodyY);
	}
	function scrollStart(){
		$('html, body').removeClass("no-scroll");
		$('.common').css('top','auto')
		bodyY = $('html,body').scrollTop(bodyY);
	}

	//글로벌 오픈/닫기
	$('.js-global-open').on('click',function(e){
		e.preventDefault();
		$('.global').toggleClass('is-active');
	});
	//모바일메뉴 오픈
	$('.js-open-m').on('click',function(e){
		var gnbState = $('.mobile-menu');
		e.preventDefault();
		if(gnbState.hasClass('is-active')){
			$('.mobile-menu').removeClass('is-active');
			$('.header').removeClass('is-open');
			scrollStart()
		}else{
			$('.mobile-menu').addClass('is-active');
			$('.header').addClass('is-open');
			scrollStop()
		}
	});
	$('.js-mouse-hover').on('mouseenter',function(e){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).addClass('is-active')
		}
	}).on('mouseleave',function(e){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).removeClass('is-active')
		}
	})

	//lnb
	$('.js-lnb').on('click',function(e){
		e.preventDefault();

		if($('.lnb__2depth-list').hasClass('is-active')){
			$('.lnb__2depth-list').removeClass('is-active');
			$('.lnb').removeClass('is-active');
		}else{
			$('.lnb__2depth-list').addClass('is-active');
			$('.lnb').addClass('is-active');
		}
	});

	$(".lnb__2depth-link").on("mouseenter", function(){
		$(this).parent(".lnb__2depth-item").addClass("is-active");
	}).on('mouseleave',function(){
		$(this).parent(".lnb__2depth-item").removeClass('is-active');
	});
	//상담
	$('.js-counsel-open').on('click',function(e){
		e.preventDefault();
		if ($('body').attr('data-mobile') == 'true'){
			var counselH = $('.counsel').outerHeight() + 1;
			var counselState = $(this).closest('.counsel');
			$(this).closest('.counsel').toggleClass('is-active');
			if(counselState.hasClass('is-active')){

			}else{
				$('.counsel').css('bottom',-counselH)

			}
		}
	});


	/* top*/
	$(function(){
		$(window).scroll(function() {
			var scrollPos = $(this).scrollTop();
			var gotopbtn = $(".top-btn");
			var footHeight = $(".footer").height();
			var docHeight = $(document).height() - $(window).height() - footHeight;
			var bannerHeight = $(window).height()*1.5;
			if (scrollPos < bannerHeight) {
				gotopbtn.removeClass('is-active');
			} else if (scrollPos >= bannerHeight && scrollPos < docHeight) {
				gotopbtn.removeClass("is-fixed");
				gotopbtn.addClass('is-active');
			} else {
				gotopbtn.addClass("is-fixed");
				gotopbtn.addClass('is-active');
			}
		});

		// gotop button
		$(".top-btn").click(function(e) {
			e.preventDefault();
			$("html,body").animate({scrollTop:0},500);
		});
	});

	$('.popup-t__scroll').mCustomScrollbar({
        theme:"dark"
    });
	//팝업
	$('.js-terms').click(function(e) {
		e.preventDefault();
		$('.popup-t').fadeIn(300);
		scrollStop();
	});
	$('.popup-t__close').on('click',function(e){
		e.preventDefault();
		$('.popup-t').fadeOut(300);
		scrollStart();
	});

	/* 유투브 주소 변경 */
	$('.content-video__link').on('click', function(e){
		e.preventDefault();
		$(this).parent().addClass('is-active');
		$(this).parent().siblings().removeClass('is-active');
		var ytb_id = $(this).data('ytbId');
		$('.content-video__iframe').remove();
		var newHtml = '<iframe src="https://www.youtube.com/embed/'+ ytb_id + '" class="content-video__iframe"></iframe>';
		$('.content-video__area').append(newHtml);
		//console.log(newHtml);
	});

});


//가슴검진 팝업 시작
var chestElem = $('.chest-select');
$(".chest-select__tit").click(function(e) {
	e.preventDefault();
	if(chestElem.hasClass('is-active')){
		chestElem.removeClass('is-active')
	}else{
		chestElem.addClass('is-active');
	}
});
$('.chest-select__item').on('click',function(){
	chestElem.removeClass('is-active');
	$('.chest-select__tit').addClass('is-select').text($(this).text());
})
var dbOpen = false;
$('.js-c-terms-close').on('click',function(e){
	e.preventDefault();
	$('.chest-terms').removeClass('is-active');
})
$('.chest-db__terms').on('click',function(e){
	e.preventDefault();
	$('.chest-terms').addClass('is-active');
})
$('.js-c-db-close').on('click',function(e){
	e.preventDefault();
	$('.chest-db').removeClass('is-active');
	$('.chest-popup__close').trigger('click');
	dbOpen = false;
})
// $('.chest-popup__btn').on('click',function(e){
// 	e.preventDefault();
// 	$('.chest-db').addClass('is-active');
// 	$('.chest-popup__over').removeClass('is-active');
// 	dbOpen = true;
// });
$('.chest-popup__close').on('click',function(e){
	e.preventDefault();
	$('.chest-popup').removeClass('is-active');
});
$('.chset-tag').on('click',function(e){
	e.preventDefault();
	$('.chest-popup').addClass('is-active');
})

$(".chest-popup__btn").on('mouseenter',function(){
	if(dbOpen == false){$('.chest-popup__over').addClass('is-active');}
}).on('mouseleave',function(){
	if(dbOpen == false){$('.chest-popup__over').removeClass('is-active');}
})
$(".js-mouse-over").on('mouseenter',function(){
	$(this).addClass('is-active');
}).on('mouseleave',function(){
	$(this).removeClass('is-active');
})
//가슴검진 팝업 끝



// 상담신청
$("#frmCounsel").validate({
	errorPlacement: function(error, element) {
        error.insertAfter(element);
    },
    messages: {
        counselNamee: "이름을 입력해주세요.",
        counselPhone: "연락처를 입력해주세요.",
        counselAgree: "개인정보 수집 및 이용 동의에 체크해주세요."
    },
    submitHandler: function() {
        var formdata = $('#frmCounsel').serialize();
    }
});

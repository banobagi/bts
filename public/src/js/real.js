$(document).ready(function(){

    // is-on
    $(".real-thum__item").each(function(e){
        if($(this).hasClass("is-on")){
            var thumImg = $(this).find("img");
            thumImg.attr("src", thumImg.attr("src").replace("off", "on"));
        }
    });

    // link hover
    $(".real-thum__link").on("mouseenter", function(){
        var thumImg = $(this).find("img");
        thumImg.attr("src", thumImg.attr("src").replace("off", "on"));
    }).on('mouseleave',function(e){
        var thumImg = $(this).find("img");
        thumImg.attr("src", thumImg.attr("src").replace("on", "off"));

        if($(this).parent(".real-thum__item").hasClass("is-on")){
            thumImg.attr("src", thumImg.attr("src").replace("off", "on"));
        }
    });
    $(window).on('load',function(){
        if ($('body').attr('data-mobile') == 'true'){
            var realOn = $('.real-thum__item.is-on').offset().left - 20;
            var realScroll = $('.real-thum__scroll');
            realScroll.animate({scrollLeft:realOn},500);
        }
    });

	$(window).scroll(function() {
		var scrollPos = $(this).scrollTop();
		var realNavBtn = $(".real-nav");
        var realVisual = $(".real-visual").outerHeight();

        if(scrollPos < realVisual){
            realNavBtn.removeClass('is-active');
        }else{
            realNavBtn.addClass('is-active');
        }
	});

});

$(document).ready(function(){
	var contentSlider = $('.js-content-slider').slick({
		dots:true,
		arrows:false,
	   infinite: true,
	   speed: 300,
	   autoplaySpeed: 500,
	   accessibility: false,
	   adaptiveHeight: true,
	   slickPrev:'.js-slider-prev',
	   dotsClass:'content-slider__page',
	   customPaging : function(slider, i) {
	        return '<a href="#" onclick="return false" class="content-slider__link">버튼</a>';
	    }
	});
	$('.js-slider-next').on('click',function(e){
		e.preventDefault();
		contentSlider.slick('slickNext')
	})
	$('.js-slider-prev').on('click',function(e){
		e.preventDefault();
		contentSlider.slick('slickPrev')
	})
});

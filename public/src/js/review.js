//검색 포커스
$('#p_keyword').focus(function() {
	$(this).parents('.photo-keyword').find('label').addClass('is-focus');
})
.blur(function() {
	$(this).parents('.photo-keyword').find('label').removeClass('is-focus');
});

// 정면, 반측면, 측면 이미지 교체
$('.photo-slide__link').on('click',function(e){
	e.preventDefault();
	var photoNum = $(this).parent().index();
	$(this).parents('.photo-slide__wrap').find('.photo-slide__item').removeClass('is-active');
	$(this).parent().addClass('is-active');
	$(this).parents('.photo-slide__wrap').find('.photo-slide__img').removeClass('is-active');
	$(this).parents('.photo-slide__wrap').find('.photo-slide__box.type-before .photo-slide__img').eq(photoNum).addClass('is-active');
	$(this).parents('.photo-slide__wrap').find('.photo-slide__box.type-after .photo-slide__img').eq(photoNum).addClass('is-active');
});

// 갤러리 이미지 교체
var slideBig = $('.photo-slide__wrap.type-big');
var slideImg = slideBig.length - 1;

var nowSlideImg = $('.photo-thum__item.is-active').index();

// if(slideImg<1) {
//   $('.slides_wrapper .img_box .arrow_left, .slides_wrapper .img_box .arrow_right').addClass('hide');
// }
function frontViw(){
	$('.photo-slide__wrap.type-big.is-active .photo-slide__img').removeClass('is-active');
	$('.photo-slide__wrap.type-big.is-active .photo-slide__box.type-before .photo-slide__img').eq(0).addClass('is-active');
	$('.photo-slide__wrap.type-big.is-active .photo-slide__box.type-after .photo-slide__img').eq(0).addClass('is-active');
}
function transferViw(){
	slideBig.removeClass('is-active');
	slideBig.eq(nowSlideImg).addClass('is-active');
	$('.photo-thum__item').removeClass('is-active');
	$('.photo-thum__item').eq(nowSlideImg).addClass('is-active');
}
$('.photo-thum__link').on('click',function(e){
	e.preventDefault();
	nowSlideImg = $(this).parents('.photo-thum__item').index();
	transferViw();
});
//슬라이드 오른쪽 클릭
$('.js-slide-next').click(function(e){
	e.preventDefault();
	if(slideImg>nowSlideImg) {
		nowSlideImg += 1;
	} else {
		nowSlideImg = 0;
	}
	transferViw();
});
//슬라이드 왼쪽 클릭
$('.js-slide-prev').click(function(e) {
	e.preventDefault();
	if(nowSlideImg == 0) {
		nowSlideImg = slideImg;
	}
	else if(slideImg>=nowSlideImg) {
		nowSlideImg -= 1;
	} else {
		nowSlideImg = slideImg;
	}
	transferViw();
});


$('.photo-slide__wrap.type-big').swipe( {
	swipeLeft:function(event, distance, duration, fingerCount, fingerData, currentDirection) {
		$('.js-slide-next').trigger('click');
	},
	swipeRight:function(event, distance, duration, fingerCount, fingerData, currentDirection) {
		$('.js-slide-prev').trigger('click');
	},
	fingers:$.fn.swipe.fingers.ALL
});



var map;
function initMapKako(){
	var mapContainer = document.getElementById('map'), // 지도를 표시할 div
    mapOption = {
        center: new daum.maps.LatLng(37.502638, 127.035733), // 지도의 중심좌표
        level: 3// 지도의 확대 레벨
    };

	map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

	var imageSrc = 'http://breastbnbg.cafe24.com/Pub/images/common/bts_marker.png', // 마커이미지의 주소입니다
		imageSize = new daum.maps.Size(62, 80), // 마커이미지의 크기입니다
		imageOption = {offset: new daum.maps.Point(27, 69)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.

	// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
	var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize, imageOption),
		markerPosition = new daum.maps.LatLng(37.502638, 127.035733); // 마커가 표시될 위치입니다

	// 마커를 생성합니다
	var marker = new daum.maps.Marker({
		position: markerPosition,
		image: markerImage // 마커이미지 설정
	});

	// 마커가 지도 위에 표시되도록 설정합니다
	marker.setMap(map);

	// 마커가 지도 위에 표시되도록 설정합니다
	marker.setMap(map);
	// 커스텀 오버레이에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
	var content = '<div class="marker__box">' +
		'  <p class="marker__txt">' +
		'    서울특별시 강남구 논현로 519 4층<br> 1588.6508 / 02.562.6001' +
		'  </p>' +
		'</div>';

	// 커스텀 오버레이가 표시될 위치입니다
	var position = new daum.maps.LatLng(37.502660, 127.03579);

	// 커스텀 오버레이를 생성합니다
	var customOverlay = new daum.maps.CustomOverlay({
		map: map,
		position: position,
		content: content,
		yAnchor: 1
	});
}


function setZoomable(zoomable) {
	map.setZoomable(zoomable);
}
function setDraggable(draggable) {
    map.setDraggable(draggable);
}
$(window).ready(function() {
	initMapKako();
	setZoomable(false);

	$('#map').on('click',function(){
		setZoomable(true);
		setDraggable(true);
	})

	var mapTop = $('#map').offset().top;
	$(window).scroll(function() {
		if ($(window).scrollTop() < mapTop || $(window).scrollTop() > mapTop) {
			setZoomable(false);
		}
	});

	$('.map-motion__fake').click(function(){
		$(this).addClass('is-hide');
		$('.map-motion__bg').addClass('is-important');
		setZoomable(true);
		setDraggable(true);
	});
	$('.map-motion__bg').on('touchstart',function(){
		$('.map-motion__bg').addClass('is-active');
	})
	$('.map-motion__bg').on('touchend',function(){
		$('.map-motion__bg').removeClass('is-active');
	})
	daum.maps.event.addListener(map, 'tilesloaded', function() {
		$('.map-motion__fake').removeClass('is-hide');
		$('.map-motion__bg').removeClass('is-important');
	});
});

function open_win()
{
  window.open('/introduce/introduce_print.html','오시는길', 'width=760, height=960, left=100px, top=100px, toolbar=no, location=no, directories=no, status=no, menubar=no, resizable=no, scrollbars=no, copyhistory=no');
}

$(document).ready(function(){

	$(window).on("load resize", function () {
		var mapH = $('.main-map__area').outerHeight();
		$('.main-map__map').height(mapH);
		function resize_fw(){
			var winW = $(window).width();
			perW = winW/3;
			$(".clinic__item").removeClass('is-active').width(perW)
		}
		if ($('body').attr('data-mobile') == 'false'){

			$('.header').on('mouseenter',function(){
				$(this).removeClass('type-main');

			}).on('mouseleave',function(){
				var headerClass = $('.header').attr('class');
				var headerClass2 = headerClass.split(' ')
				if(headerClass2[1] != 'is-fix'){
					$(this).addClass('type-main');
				}
			});
			//detail
			resize_fw()
			$(".js-clinic__item").on('mouseenter',function(){
				$(".js-clinic__item").removeClass("is-active")
				$(".js-clinic__item:not(.is-active)").addClass('is-dark')
				.stop().animate({"width":(perW-(228/3))+"px"},300);
				$(this).addClass("is-active")
				.stop().animate({"width":(perW+228)+"px"},300);
				$(this).removeClass("is-dark").siblings().addClass("is-dark");
			})
			$('.js-clinic').on('mouseleave',function(){
				$(".js-clinic__item").removeClass("is-active is-dark").stop().animate({"width":perW},300);
			});
		}else{
			$(".js-clinic__item").off('mouseenter')
		}

	});

	$('.js-main-slider').cycle({
		swipe:true,
		fx:'scrollHorz',
	    slides:'.main-visual__item',
		prev:'.js-main-prev',
		next:'.js-main-next',
		maxZ:10,
		pager:'.main-visual__page',
		pagerTemplate:'<span class="main-visual__bullet"></span>',
		pagerActiveClass:'is-active',
		timeout:6000
	});
	$('.js-list-slide').slick({
	   infinite: true,
	   speed: 300,
	   autoplaySpeed: 500,
	   accessibility: false,
	   centerMode: true,
	   centerPadding: '80px',
	   responsive:[
		   {
				breakpoint: 680,
				settings: {
					centerMode: true,
					centerPadding: '35px'
				}
			}
	   ]
	});

	function gnbMotion(){
		var winH = $(window).height();
		if ($(window).scrollTop()<winH) {
			var isBody = $('body').attr('class');
			if(isBody != "no-scroll"){
				$('.header').stop().addClass('type-main').removeClass('is-fix');;
			}
		}
		else {
			$('.header').stop().removeClass('type-main').addClass('is-fix');
		}
	}
	gnbMotion();
	$(window).scroll(function() {
		gnbMotion();
	});



	/* setCookie function */
	function setCookie(cname, value, expire) {
	   var todayValue = new Date();
	   // 오늘 날짜를 변수에 저장

	   todayValue.setDate(todayValue.getDate() + expire);
	   document.cookie = cname + "=" + encodeURI(value) + "; expires=" + todayValue.toGMTString() + "; path=/;";
	}
	// Get cookie function
	function getCookie(name) {
	   var cookieName = name + "=";
	   var x = 0;
	   while ( x <= document.cookie.length ) {
	      var y = (x+cookieName.length);
	      if ( document.cookie.substring( x, y ) == cookieName) {
	         if ((lastChrCookie=document.cookie.indexOf(";", y)) == -1)
	            lastChrCookie = document.cookie.length;
	         return decodeURI(document.cookie.substring(y, lastChrCookie));
	      }
	      x = document.cookie.indexOf(" ", x ) + 1;
	      if ( x == 0 )
	         break;
	      }
	   return "";
	}




	// 하루동안 안열기 쿠키 저장
	$(function() {
	   var closeTodayBtn = $('.js-popup-m-today');
	   var closeTodayBtn2 = $('.js-popup-m-today2');
	   var closeTodayBtn3 = $('.js-popup-m-today3');

	   // 버튼의 클래스명은 closeTodayBtn

	   closeTodayBtn.click(function(e) {
	      setCookie( "popup20180910", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-first').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });

	   closeTodayBtn2.click(function(e) {
	      setCookie( "popup20180912", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-second').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });

	   closeTodayBtn3.click(function(e) {
	      setCookie( "popup20181226", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-third').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });

	});


	//쿠키체크후 팝업열기
	var result = getCookie('popup20180910');
	var result2 = getCookie('popup20180912');
	var result3 = getCookie('popup20181226');
	if (result != 'end') {
		$('.popup-m.type-first').addClass('is-active')
	}
	if (result2 != 'end') {
		$('.popup-m.type-second').addClass('is-active')
	}
	if (result3 != 'end') {
		$('.popup-m.type-third').addClass('is-active')
	}
	$('.js-popup-m-close').on('click',function(e){
		e.preventDefault();
		$(this).closest('.popup-m').removeClass('is-active')
	});


});

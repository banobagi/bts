/* 의료진 소개 */
$(window).on('load',function(){
	try {
		TweenMax.fromTo('.medical__list', 1, {opacity:0,y:40},{opacity:1,y:0,delay:.3});
	} catch(error) {}
});

var bodyY;
function scrollStop(){
	bodyY = $(window).scrollTop();
	$('html, body').addClass("no-scroll");
	$('.common').css("top",-bodyY);
}
function scrollStart(){
	$('html, body').removeClass("no-scroll");
	$('.common').css('top','auto')
	bodyY = $(window).scrollTop(bodyY);
}

$('.medical__link').on('click',function(e){
	e.preventDefault();
	var docID = $(this).attr('id');
	$('.medical-view__item').removeClass('is-active');
	$('#'+docID+'_view').addClass('is-active');

	TweenMax.to('.medical-view',1,{'top':'0%',ease: Expo.easeInOut});
	TweenMax.to('.medical-view__close',.5,{opacity:1,rotation:180, delay:1,});
	scrollStop();
});
// 의료진 상세보기 닫기
$('.medical-view__close').on('click',function(e){
	e.preventDefault();
	$('body').removeClass('no-scroll');
	TweenMax.to('.medical-view',1,{'top':'100%',ease: Expo.easeInOut});
	TweenMax.to('.medical-view__close',0,{opacity:0,rotation:0, delay:0.8});
	scrollStart()
});

// 의료진 이전/다음
var $docItem = $('.medical-view__item');
var docTotal = $docItem.length;

function docReset(){
	$('.tab-v4__item, .tab-v4__content').removeClass('is-active');
	$('.tab-v4__item:first-child, .tab-v4__content:first-of-type').addClass('is-active');
}
$('.js-doctor-prev').click(function(e){
	e.preventDefault();
	var prevIndex = $('.medical-view__item.is-active').index() - 1;
	if (prevIndex < 0) {
		prevIndex = docTotal - 1;
	}
	docReset();
	$docItem.removeClass('is-active').eq(prevIndex).addClass('is-active');;
});

$('.js-doctor-next').click(function(e){
	e.preventDefault();
	var nextIndex = $('.medical-view__item.is-active').index() + 1;
	if (docTotal == nextIndex) {
		nextIndex = 0;
	}
	docReset();
	$docItem.removeClass('is-active').eq(nextIndex).addClass('is-active');
});
//의료진소개 약력/저서 스크롤
$(window).load(function(){
	$('.js-scroll').mCustomScrollbar({
		theme:"minimal-dark"
	});
});

//의료진소개 쪽 탭 링크
$('.js-tab-link2').on('click',function(e){
	e.preventDefault();
	var tab_id = $(this).parent().attr('data-tab');
	$(this).parent().addClass('is-active').siblings().removeClass('is-active');
	$(this).closest('.medical-view__item').find("."+tab_id).addClass('is-active').addClass('is-active').siblings().removeClass('is-active');
});

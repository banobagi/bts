/*
npm i -D gulp gulp-plumber gulp-connect gulp-ejs-locals gulp-compass gulp-autoprefixer gulp-jshint jshint-stylish gulp-watch gulp-kss gulp-clean
npm i -D gulp-imagemin
npm i -D imagemin-pngquant
*/

var gulp = require('gulp'),
	clean = require('gulp-clean'),
	watch = require('gulp-watch'),
	plumber = require('gulp-plumber'),
	// compass = require('gulp-compass'),
	autoprefixer = require('gulp-autoprefixer'),
	connect = require('gulp-connect'),
	ejslocals = require('gulp-ejs-locals'),
	jshint = require('gulp-jshint'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	kss = require('gulp-kss'),
	sourcemaps = require('gulp-sourcemaps');
	scss = require('gulp-sass');

var fs = require('fs');

// Path
var src = './public/src';
var dist = './public/dist';
var paths = {scss : (src+'/scss/**/*.{scss,sass}') };

// Server
gulp.task('serve', function() {
	connect.server({
		root: dist,
		port: 8000,
		livereload: true,
		open: {
			browser: 'chrome'
		}
	});
});

var scssOptions = { /** * outputStyle (Type : String , Default : nested) * CSS의 컴파일 결과 코드스타일 지정 * Values : nested, expanded, compact, compressed */
	outputStyle : "compressed", /** * indentType (>= v3.0.0 , Type : String , Default : space) * 컴파일 된 CSS의 "들여쓰기" 의 타입 * Values : space , tab */
	indentType : "tab", /** * indentWidth (>= v3.0.0, Type : Integer , Default : 2) * 컴파일 된 CSS의 "들여쓰기" 의 갯수 */
	indentWidth : 1, // outputStyle 이 nested, expanded 인 경우에 사용 /** * precision (Type : Integer , Default : 5) * 컴파일 된 CSS 의 소수점 자리수. */
	precision: 6, /** * sourceComments (Type : Boolean , Default : false) * 컴파일 된 CSS 에 원본소스의 위치와 줄수 주석표시. */
	sourceComments: true
};
gulp.task('scss:compile', function () {
	return gulp
	// SCSS 파일을 읽어온다.
	.src(paths.scss)
	// 소스맵 초기화(소스맵을 생성)
	.pipe(sourcemaps.init())
	// SCSS 함수에 옵션갑을 설정, SCSS 작성시 watch 가 멈추지 않도록 logError 를 설정
	.pipe(scss(scssOptions).on('error', scss.logError))
	// 위에서 생성한 소스맵을 사용한다.
	.pipe(sourcemaps.write())
	// 목적지(destination)을 설정
	.pipe(gulp.dest(dist + '/css'))
});


// SASS -> CSS
gulp.task('compass', function() {
	gulp
		.src(src+'/scss/**/*.{scss,sass}')
		.pipe( plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
			}
		}) )
		.pipe(compass({
			sourcemap:true,
			css: dist+'/css',
			sass: src+'/scss',
			style: 'compact' // nested, expaned, compact, compressed
		}))
		.pipe(autoprefixer({
			browsers: ['last 3 versions','IE 8','android 2.3'],
			cascade: false
		}))
		.pipe( gulp.dest(dist+'/css') )
		.pipe(connect.reload())
});

gulp.task('kss', function() {
	gulp.src(src+'/scss/**/*')
	.pipe(kss({
		overview: src+'/styleguide-template/styleguide.md',
		templateDirectory: src+'/styleguide-template'
	}))
	.pipe(gulp.dest(dist+'/styleguide/'))
	.pipe(connect.reload())
});

// ejs -> HTML
gulp.task('ejs', function() {
	gulp
		.src([src+'/ejs/**/*.ejs',  '!' + src+'/ejs/**/_*.ejs'])
		.pipe(ejslocals(
			{jsonData: JSON.parse(fs.readFileSync(src+'/ejs/index.json'))},
			{ext: '.html'}
		))
		.pipe(plumber({
			handleError: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(gulp.dest(dist+'/html'))
		.pipe(connect.reload())
});

// js hint
gulp.task('js:hint', function () {
	gulp
		.src([src+'/js/*.js',  '!' + src+'/js/libs/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(gulp.dest(dist+'/js'))
		.pipe(connect.reload())
});

// image
gulp.task('imgmin', function() {
	gulp
		.src([src+'/images/**/*.{png,jpg,gif}'])
		.pipe(imagemin({
			progressive: true,
			interlaced:true,
			use: [pngquant()]
		}))
		.pipe(gulp.dest(dist+'/images'))
		.pipe(connect.reload())
});

// font
gulp.task('font', function () {
	gulp
		.src([src+'/fonts/**/*'])
		.pipe(gulp.dest(dist+'/fonts'))
});

// library
gulp.task('libs', function () {
	gulp
		.src([src+'/js/libs/**/*'])
		.pipe(gulp.dest(dist+'/js/libs/'))
		.pipe(connect.reload())
	gulp
		.src([src+'/css/libs/**/*'])
		.pipe(gulp.dest(dist+'/css/libs/'))
		.pipe(connect.reload())
});

// index
gulp.task('index', function () {
	gulp
		.src([src+'/index.html'])
		.pipe(gulp.dest(dist))
});

//clean 작업 설정

gulp.task('clean', function(){
	return gulp.src([dist+'/*',dist+'/*.html','!' + dist+'/.git'], {read: false})
		.pipe(clean());
});

// Watch task
gulp.task('watch',[], function() {
	watch(src+'/ejs/**/*.ejs', function() {
		gulp.start('ejs');
	});
	// watch(src+'/scss/**/*.{scss,sass}', function() {
	// 	gulp.start(['compass','kss']);
	// });
	watch([src+'/js/*.js',  '!' + src+'/js/libs/*.js'], function() {
		gulp.start('js:hint');
	});
	 // watch(src+'/images/**/*.{png,jpg,gif}', function() {
	 // gulp.start('imgmin');
	 // });
	//watch(src+'/index.html', function() {
	//	gulp.start('index');
	//});
	watch([src+'/js/libs/**/*', src+'/css/libs/**/*'], function() {
		gulp.start('libs');
	});
	watch(src+'/scss/**/*.{scss,sass}', function() {
		gulp.start(['scss:compile']);
	});
});

gulp.task('default', ['serve','scss:compile','ejs','js:hint','imgmin','watch','font','index','libs','kss']);
